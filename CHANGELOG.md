# CHANGELOG



## v0.1.1 (2023-10-30)

### Fix

* fix: change settings of semantic-release ([`e334575`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/e33457577e399e473c687b6b82b71ef4bd692ecc))


## v0.1.0 (2023-10-30)

### Chore

* chore: 0.1.0

Automatically generated by python-semantic-release ([`587c8ca`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/587c8ca8fbc525d7f20ca727a033a116dd2c661a))

### Feature

* feat: add semantic-release pipeline ([`63bec46`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/63bec4615cfa4dc74d6271824dab62a7a8c513ae))


## v0.0.0 (2023-10-30)

### Feature

* feat: add semantic release ([`b560800`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/b560800742bd20cb9f0013188552eb062686f355))

* feat: specify configuration of semantic-release ([`c41d3a3`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/c41d3a35685637d781fe4ec052ea5be5a52a7520))

### Fix

* fix: dependency installation ([`76dd77c`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/76dd77c2b5b8b80029bf0a0e622ea1e2ce186c59))

* fix: wrong dependency ([`e765634`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/e765634068a2bf06fd169303b505ac5d0ff6507f))

### Unknown

* fix ([`4e3ab9e`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/4e3ab9e9cac60ffecca342956a808ac99e5813bd))

* print environment variable ([`ce2df3e`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/ce2df3ede9655621bdec579cc79db14961039892))

* init ([`20b7199`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/20b719938599b5eef1b3971b1fb2d759414ddeeb))

* Initial commit ([`9954c19`](https://gitlab.com/sanoj1020/test-pipeline/-/commit/9954c19b0227677a19e89374abdf36e1461d9de5))
